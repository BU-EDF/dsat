#include <AddressTable.hh>
#include <AddressTableException.hh>

#include <cstddef>
#include <fstream>
#include <boost/filesystem.hpp> //for path
#include <boost/tokenizer.hpp> //tokenizer
#include <boost/algorithm/string.hpp> //iequals
#include <stdlib.h>  //strtoul & getenv
#include <boost/regex.hpp> //regex
#include <boost/algorithm/string/case_conv.hpp> //to_upper

#define MAX_FILE_LEVEL 20
#define ADDRESS_TABLE_PATH "ADDRESS_TABLE_PATH" 

void AddressTable::LoadFile(std::string const & filename,
			    std::string const & prefix,
			    uint32_t offset,
			    Item * itemParent){
  if(0 == filename.size()){
    Exception::INVALID_STATE e;
    e.Append("no filename is given");
    throw e;
  }

  std::string triedFilenames = filename;
  //try to open file
  std::ifstream inFile(filename.c_str());
  if(inFile.is_open() && (fileLevel == 0)){
    topPath = boost::filesystem::path(filename).parent_path().string();
  }
  //If the file doesn't open, try some tricks

  
  //======================================================
  //Try to use address table path if it exists
  if(!inFile.is_open() &&
     getenv(ADDRESS_TABLE_PATH) != NULL){
    std::string envBasedFilename;
    envBasedFilename = getenv(ADDRESS_TABLE_PATH);
    envBasedFilename += "/";
    envBasedFilename += filename;
    
    triedFilenames += " ";
    triedFilenames += envBasedFilename;
    
    inFile.open(envBasedFilename.c_str());
    if(inFile.is_open() && (fileLevel == 0)){
      topPath = boost::filesystem::path(envBasedFilename).parent_path().string();
    }
  }
  
  //======================================================
  //Try using the top path if set
  if(!inFile.is_open() &&
     !topPath.empty()){
    std::string topBasedFilename;
    topBasedFilename = topPath;
    topBasedFilename += "/";
    topBasedFilename += filename;
    
    triedFilenames += " ";
    triedFilenames += topBasedFilename;
    
    inFile.open(topBasedFilename.c_str());
  }
  
  //If the file failed to open, throw an exception
  if (!inFile.is_open()){
    Exception::BAD_FILE e;
    e.Append("File not found: ");
    e.Append(filename.c_str());
    e.Append("\nTried: ");
    e.Append(triedFilenames.c_str());
    throw e;        
  }

  
 
  
  const size_t bufferSize = 1000;
  char buffer[bufferSize + 1];
  memset(buffer,0,bufferSize+1);
  uint32_t lineNumber = 1;

  
  while(! inFile.eof()){
    inFile.getline(buffer,bufferSize);
    ProcessLine(std::string(buffer),lineNumber,prefix,offset,itemParent);
    lineNumber++;
  }
}


void AddressTable::ProcessName(Item * item,
			       std::string const & prefix,
			       std::string name){
  boost::to_upper(name);       	
  
  if(!prefix.empty()){
    //we have a prefix to add on to the name
    name = prefix+std::string(".")+name;
  }
  
  while( (name.size() > 0) && ('.' == name[name.size()-1])) {
    //If the trailing entry is a dot (level marker) drop it as this entry just means the prefix
    name.erase(name.size()-1);
  }
  
  
  if(name.size() == 0){
    //We have an emtpy name, this is bad and we should throw
    Exception::INVALID_NAME e;
    e.Append("Empty name");
    throw e;    	  
  }
  item->name.assign(name); 
}
void AddressTable::ProcessAddress(Item * item,
				  uint32_t offset,
				  std::string const & value){
  //itToken means we don't have to check for size of string
  item->address = strtoul(value.c_str(),NULL,0) + offset;
};
void AddressTable::ProcessMask(Item * item,
			       std::string const & value){
  item->mask = strtoul(value.c_str(),NULL,0);      
  //stolen from https://graphics.stanford.edu/~seander/bithacks.html#ZerosOnRightParallel
  //Find the bitshift offset
  {
    unsigned int v = item->mask;      // 32-bit word input to count zero bits on right
    unsigned int c = 32; // c will be the number of zero bits on the right
    v &= -signed(v);
    if (v) {c--;}
    if (v & 0x0000FFFF) {c -= 16;}
    if (v & 0x00FF00FF) {c -= 8;}
    if (v & 0x0F0F0F0F) {c -= 4;}
    if (v & 0x33333333) {c -= 2;}
    if (v & 0x55555555) {c -= 1;}
    item->offset = c;
  }  
}
void AddressTable::ProcessInclude(Item * item,
				  std::string filename,
				  Item * itemParent){
  //we have an include file, append all following tokens together to use as the filename.
  //NO SPACES IN FILENAME
  
  fileLevel++;
  if(fileLevel <= MAX_FILE_LEVEL){
    LoadFile(filename,
	     item->name,
	     item->address,
	     itemParent);	  
  }else{
    Exception::MAX_INCLUDE_FILE_DEPTH e;
    e.Append("File: ");
    e.Append(filename);
    e.Append(" at prefix ");
    e.Append(item->name);
    e.Append(" is too deep\n");
    throw e;
  }
  fileLevel--;	
}
void AddressTable::ProcessMode(Item * item,
			       std::string const & value){
  item->mode = 0;
  
  //Read
  if(value.find('r') != std::string::npos){
    item->mode |= Item::READ;
  }else if (value.find('R') != std::string::npos){
    item->mode |= Item::READ;
  }
  
  //Write
  if(value.find('w') != std::string::npos){
    item->mode |= Item::WRITE;
  }else if (value.find('W') != std::string::npos){
    item->mode |= Item::WRITE;
  }
  
  //Action
  if(value.find('a') != std::string::npos){
    item->mode |= Item::ACTION;
  }else if (value.find('A') != std::string::npos){
    item->mode |= Item::ACTION;
  }  
}
void AddressTable::ProcessUser(Item * item,
			       boost::tokenizer<boost::char_separator<char> > tokens,
			       boost::tokenizer<boost::char_separator<char> >::iterator & itToken,
			       size_t & iToken,
			       std::string const & activeLine,
			       size_t lineNumber
			       ){
  //repeated arguments will be over-written
  
  //Find if this is an argument or a flag, flags have no equal signs
  if(itToken->find('=') == std::string::npos){
    //Create an entry for this with no data
    item->user[*itToken];
    iToken++;
  }else{
    //Get the name of the user value
    size_t equalSignPos = itToken->find('=');
    //make sure there isn't a """ before the =
    if(itToken->find('"') != std::string::npos){
      if(itToken->find('"') < equalSignPos){
	Exception::BAD_TOKEN e;
	e.Append("Malformed token : ");
	e.Append(itToken->c_str());
	e.Append(" on line ");	    
	char numberBuffer[14] = "\0\0\0\0\0\0\0\0\0\0\0\0";
	snprintf(numberBuffer,12,"%zu\n",lineNumber);
	e.Append(numberBuffer);
	e.Append("Bad line: ");
	e.Append(activeLine);
	e.Append("\n");
	throw e;
      }
    }
    //cache the name of the user field
    std::string name = itToken->substr(0,equalSignPos);
    //Parse the rest of the user value if there is more size
    if(itToken->size()-1 == equalSignPos){
      Exception::BAD_TOKEN e;
      e.Append("Malformed token : ");
      e.Append(itToken->c_str());
      e.Append(" on line ");	    
      char numberBuffer[14] = "\0\0\0\0\0\0\0\0\0\0\0\0";
      snprintf(numberBuffer,12,"%zu\n",lineNumber);
      e.Append(numberBuffer);
      throw e;	  
    }
    //star this user field's data (data after the '=' char)
    std::string val = itToken->substr(equalSignPos+1);
    if(val[0] != '"'){
      //We have a simple entry that has no quotes
      item->user[name] = val;
      iToken++;
    }else{
      //We have a quoted value
      val.erase(0,1); //remove the quote
      
      //Check if this is still a simple entry, but with quotes	  
      if(val.find('"') != std::string::npos){
	//We have another quote, remove it and everything after it
	val = val.substr(0,val.find('"'));
      }else{
	//We have a complicated value
	itToken++;
	while(itToken != tokens.end()){
	  val.append(" ");
	  val.append(*itToken);
	  if((*itToken)[itToken->size() -1] == '"'){	      
	    //stop now that we've reached the closing quote
	    //remove it from the string
	    val.erase(val.size()-1);
	    break;
	  }
	  iToken++;
	  itToken++;
	}
      }
      //convert "\n" to '\n'
      while(val.find("\\n") != std::string::npos){
	val.replace(val.find("\\n"),2,std::string("\n"));
      }
      item->user[name] = val;
    }
  }
}

void AddressTable::ProcessLine(std::string const & line,
			       size_t lineNumber,
			       std::string const & prefix,
			       uint32_t offset,
			       Item * itemParent){
  //First, ignore commments
  std::string activeLine = line.substr(0,line.find('#'));
  

  //Tokenize the activeLine
  boost::char_separator<char> sep(" ");
  boost::tokenizer<boost::char_separator<char> > tokens(activeLine,sep);
  boost::tokenizer<boost::char_separator<char> >::iterator itToken = tokens.begin();
  
 
  //crate a new Item;
  Item * item = new Item;
  item->parent = itemParent;
  size_t iToken = 0;
  //Loop over tokenized line
  for(; itToken != tokens.end(); itToken++){
    switch (iToken){      
    case 0:
      //=========================================================================
      //Pos 0
      {// keep name out of everyone else's scope
	//Assign name
	ProcessName(item,		    
		    prefix,
		    *itToken);
	iToken++;
      }
      break;
    case 1:
      //=========================================================================
      //Pos 1
      //Assign address and apply any offset
      ProcessAddress(item,
		     offset,
		     *itToken);
      iToken++;
      break;
    case 2:
      //=========================================================================
      //Pos 2
      
      //Check if this is an include line
      if(!isdigit((itToken->c_str()[0]))){
	ProcessInclude(item,
		       *itToken,
		       itemParent);
	//Delete fake partial item
	delete item;
	//return to move on to the next line of the main file
	return;
      }else{
	//Assign mask
	ProcessMask(item,*itToken);
	iToken++;
      }
      break;
    case 3:
      //=========================================================================
      //Pos 3
      
      //Assign mode
      ProcessMode(item,*itToken);
      iToken++;      
      break;
    default:
      //=========================================================================
      //Pos > 3
      
      //parse user arguments
      ProcessUser(item,
		  tokens,itToken,iToken,
		  activeLine,lineNumber);
      break;  
    }
    if(itToken == tokens.end()){
      break;
    }
  }

  //Decide what to do with this object    
  if(iToken >= 4){
    AddEntry(item);
  }else{
    delete item;
  }
}


