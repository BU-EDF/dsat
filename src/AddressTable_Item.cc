#include "Item.hh"
#include <AddressTable.hh>
#include <boost/algorithm/string/predicate.hpp>
#include <fstream>
#include <AddressTableException.hh>
#include <boost/tokenizer.hpp> //tokenizer
#include <stdlib.h>  //strtoul & getenv
#include <boost/regex.hpp> //regex
#include <boost/algorithm/string/case_conv.hpp> //to_upper

const char * AddressTable::SC_key_conv = "sc_conv";

std::vector<Item const *> AddressTable::GetChildren (){
  std::vector<Item const *> ret;
  for(auto it = children.begin();it!=children.end();it++){
    ret.push_back(*it);
  }
  return ret;
}

Item const * AddressTable::GetItem(std::string const & registerName){
  std::map<std::string,Item *>::iterator itNameItem = nameItemMap.find(registerName);
  if(itNameItem == nameItemMap.end()){
    Exception::INVALID_NAME e;
    e.Append("Can't find item with name \"");
    e.Append(registerName.c_str());
    e.Append("\"");
    throw e;    
  }
  Item * item = itNameItem->second;
  return item;
}

Item * AddressTable::FindExisting(Item * item,std::string const & name){
  //Look for a name match with the parent's cildren.

  Item * ret = NULL;

  if(item->parent == NULL){
    return ret;
  }
  
  auto siblings = item->parent->children;
  auto itSibling = siblings.begin();
  for(;itSibling != siblings.end();
      itSibling++){
    if(((*itSibling) != item) &&     //check that this isn't ourselves.
       (name == (*itSibling)->name)){ //check the names for a match
      ret = *itSibling;
      break;
    }
  }
  return ret;
}

void AddressTable::AddEntry(Item * item){
  //Check for null item
  if(item == NULL){
    Exception::NULL_POINTER e;
    e.Append("Null Item pointer passed to AddEntry\n");
    throw e;
  }


  //Check if the name has more than one level in it.
  if(item->name.find(".") != std::string::npos){
    //==========================================================================
    //This is a compound name
    //==========================================================================

    //Check if this level has already been created
    std::string levelName = item->name.substr(0,item->name.find("."));

    //Get the existing level (if it exists)
    Item * level = FindExisting(item,levelName);
    if(!level){
      //this doesn't exist, create it.
      level = new Item;
      //Fill it with the info from item before we promote item.
      level->name = levelName;
      level->address = item->address;
      level->parent = item->parent;
      //Add this level to item's parent
      if (level->parent != NULL){	
	level->parent->children.push_back(level);
      }else{
	//Parent is null, we must be at the highest level.
	this->children.push_back(level);
      }
    }

    //Set item's parent to level
    item->parent = level;
    //clean up item's name
    item->name = item->name.substr(item->name.find(".")+1);

    //Add item to this level
    //    level->children.push_back(item);

    AddEntry(item);
  }else{
    //==========================================================================
    //This is a normal name
    //==========================================================================

    //Check for invalid Mode
    if((item->mode & Item::WRITE) &&
       (item->mode & Item::ACTION)){    
      Exception::BAD_MODE e;
      e.Append("AddEntry called with both WRITE and ACTION modes\n");
      throw e;
    }
    
    //This function now owns the memory at item
    
    //Add the item to the address map and check that it doesn't conflict with 
    std::map<uint32_t,std::vector<Item*> >::iterator itAddressItem = addressItemMap.find(item->address);
    if(itAddressItem == addressItemMap.end()){
      //This is the first entry at this address.
      //Create the entry in addressItemMap and then push_back with our item.
      addressItemMap[item->address].push_back(item);
      //Update the iterator to the newly inserted item
      itAddressItem = addressItemMap.find(item->address);
    }else{
      std::vector<Item*> & addressItems = itAddressItem->second;
      //Check each entry for an overlap in masks.  This is only ok if one is a subset, via it's name, of the other
      for(size_t iItem = 0; iItem < addressItems.size();iItem++){
	if(addressItems[iItem]->mask & item->mask){
	  //free for all now
	  
	  
	  //These items overlap in mask
	  //CHeck that one or the other contains the other starting at pos 0.
	  //in other words, that they are the same up until the end of the shorter one
	  //	if( (addressItems[iItem]->name.find(item->name) != 0) &&
	  //	    (item->name.find(addressItems[iItem]->name) != 0) ) {
	  //	  Exception::BAD_MODE e;
	  //	  e.Append("Entry: ");
	  //	  e.Append(item->name.c_str());
	  //	  e.Append(" conflicts with entry ");
	  //	  e.Append(addressItems[iItem]->name.c_str());
	  //	  e.Append("\n");
	  //	  throw e;	
	  //	}
	}      
      }
      addressItems.push_back(item);
    }
    
    //Add the item to the list of addresses
    std::string itemFullName = item->GetFullName();
    std::map<std::string,Item *>::iterator itNameItem = nameItemMap.find(itemFullName);
    if(itNameItem == nameItemMap.end()){
      //Add this entry and everything is good. 
      nameItemMap[itemFullName] = item;
    }else{
      //There was a collision in entry name, remote the newly added element and throw an exception
      
      //delete the addition to the vector
      std::vector<Item*> & addressItems = itAddressItem->second;
      for(size_t iItem = 0; iItem < addressItems.size();iItem++){
	if(addressItems[iItem] == item){
	  //Found what we just added, removing
	  addressItems.erase(addressItems.begin()+iItem);
	}
      }
      // throw exception about collission
      Exception::NAME_COLLISION e;
      e.Append("Item ");
      e.Append(itemFullName.c_str());
      e.Append(" already existed\n");
      
      //delete the item
      delete item;
      
      throw e;	    
    }
    if(item->parent != NULL){
      item->parent->children.push_back(item);
    }else{
      //This is at the top level
      this->children.push_back(item);
    }
    //Everything is good, we've added it
  }
}


