/*
 * Python bindings for DSAT class
 */

#include <AddressTable.hh>
#include <AddressTableException.hh>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;


PYBIND11_MODULE(libdsat, m) {
  py::class_<Item>(m,"Item")
    .def(py::init<>())
    .def("GetName",    &Item::GetName)
    .def("GetAddress", &Item::GetAddress)
    .def("GetMask",    &Item::GetMask)
    .def("GetOffset",  &Item::GetOffset)
    .def("GetMode",    &Item::GetMode)
    .def("GetUser",    &Item::GetUser);

  py::enum_<Item::ModeMask>(m, "ModeMask")
    .value("EMPTY",    Item::EMPTY)
    .value("READ",     Item::READ)
    .value("WRITE",    Item::WRITE)
    .value("ACTION",   Item::ACTION)
    .export_values();

  py::class_<AddressTable>(m,"AddressTable")
    .def(py::init<std::string const &>())
    .def("GetItem",      &AddressTable::GetItem)
    .def("GetTagged",    &AddressTable::GetTagged)
    .def("GetNames",     &AddressTable::GetNames)
    .def("GetAddresses", &AddressTable::GetAddresses)
    .def("GetTables",    &AddressTable::GetTables);

  //Exceptions
  py::register_exception<Exception::BAD_FILE>(m,               "BAD_FILE");              
  py::register_exception<Exception::INVALID_NAME>(m,           "INVALID_NAME");          
  py::register_exception<Exception::INVALID_STATE>(m,          "INVALID_STATE");         
  py::register_exception<Exception::BAD_TOKEN>(m,              "BAD_TOKEN");             
  py::register_exception<Exception::NULL_POINTER>(m,           "NULL_POINTER");          
  py::register_exception<Exception::BAD_MODE>(m,               "BAD_MODE");              
  py::register_exception<Exception::NAME_COLLISION>(m,         "NAME_COLLISION");        
  py::register_exception<Exception::BAD_REGEX>(m,              "BAD_REGEX");             
  py::register_exception<Exception::MAX_INCLUDE_FILE_DEPTH>(m, "MAX_INCLUDE_FILE_DEPTH");
  py::register_exception<Exception::BAD_BLOCK_WRITE>(m,        "BAD_BLOCK_WRITE");       

}
