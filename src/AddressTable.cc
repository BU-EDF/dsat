#include <AddressTable.hh>
#include <fstream>
#include <AddressTableException.hh>
#include <stdlib.h>  //strtoul & getenv


AddressTable::AddressTable(std::string const & addressTableName){
  fileLevel = 0;
  LoadFile(addressTableName,
	   "", //starting prefix is ""
	   0,   //starting offset is 0
	   (Item *) this
	   );
}

AddressTable::~AddressTable(){
  //clean up items
  for(std::map<uint32_t,std::vector<Item*> >::iterator itMap = addressItemMap.begin();
      itMap != addressItemMap.end();
      itMap++){
    for(size_t iItem = 0; iItem < itMap->second.size();iItem++){
      delete itMap->second[iItem];
    }
  }


}
