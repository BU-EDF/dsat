#include <Item.hh>

Item::Item(){
  address = 0;
  mask = 0;
  offset = 0;
  mode = 0;
  parent = NULL;
}


std::string const &  Item::GetName() const {
  return name;
}
std::string Item::GetParentName() const {
  if(parent == NULL){
    return std::string("");
  }
  return parent->GetFullName();
}
std::string Item::GetFullName(size_t recursionLevels) const {
  std::string ret;
  if(recursionLevels != 0){
    if(parent != NULL){
      ret = parent->GetFullName();
      if(!ret.empty()){
	ret += ".";
      }
    }
  }else{
    //TODO: Throw an exception here
  }
  ret += name;
  return ret;
}
uint32_t    Item::GetAddress() const {
  return address;
}
uint32_t    Item::GetMask() const {
  return mask;
}
uint8_t     Item::GetOffset() const {
  return offset;
}
uint8_t     Item::GetMode() const {
  return mode;
}
std::unordered_map<std::string,std::string> const & Item::GetUser() const {
  return user;
}
Item const * Item::GetParent()                                      const {
  return parent;
}
std::vector<Item const *> const Item::GetChildren()                       const{
  std::vector<Item const *> ret;
  for( auto it = children.begin();it != children.end();it++){
    ret.push_back(*it);
  }
  return ret;
}
