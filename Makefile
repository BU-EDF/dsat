SHELL = bash
PackagePath = $(shell pwd)

#Addresstable
LIBRARY_ATABLE_SO_FILE = lib/libdsat.so
LIBRARY_ATABLE_SOURCES = $(wildcard src/AddressTable*.cc)
LIBRARY_ATABLE_SOURCES += $(wildcard src/Item*.cc)
LIBRARY_ATABLE_OBJECT_FILES = $(patsubst src/%.cc, obj/%.o, ${LIBRARY_ATABLE_SOURCES})

#exception
LIBRARY_EXCEPTION_SO_FILE = external/Exception/lib/libException.so

#python
PYBIND11_LIBRARY = lib/libdsat$(shell python3-config --extension-suffix)
PYBIND11_SOURCES = $(wildcard src/pybind*)
PYBIND11_OBJECT_FILES = $(patsubst src/%.cc, obj/%.o, ${PYBIND11_SOURCES})

#BUILD VARIABLES
INCLUDE_PATH = \
	-Iinclude \
	-Iexternal/Exception/include \
	$(shell python3-config --includes) \

#	$(shell python3 -m pybind11 --includes) \

CPP_FLAGS = -std=c++17 -g -O3 -rdynamic -Wall -MMD -MP -fPIC ${INCLUDE_PATH} -Werror -Wno-literal-suffix
CPP_FLAGS +=-fno-omit-frame-pointer -Wno-ignored-qualifiers -Werror=return-type -Wextra -Wno-long-long -Winit-self -Wno-unused-local-typedefs  -Woverloaded-virtual


LIBRARY_PATH = \
	-Llib \
	-Lexternal/Exception/lib \

LINK_LIBRARY_FLAGS = -Xlinker "--no-as-needed" -shared -fPIC -Wall -g -O3 -rdynamic ${LIBRARY_PATH} -Wl,-rpath=${PackagePath}/lib 

LINK_LIBRARIES = \
	-lboost_regex \
	-lboost_filesystem \
	-lException \



#RULES
defult: build
clean: _cleanall
_cleanall:
	rm -rf obj
	rm -rf lib

all: _all
build: _all
buildall: _all
_all: ${LIBRARY_ATABLE_SO_FILE} ${PYBIND11_LIBRARY}

${LIBRARY_ATABLE_SO_FILE}: ${LIBRARY_EXCEPTION_SO_FILE} ${LIBRARY_ATABLE_OBJECT_FILES}
	g++ ${LINK_LIBRARY_FLAGS} ${LIBRARY_ATABLE_OBJECT_FILES} ${LINK_LIBRARIES} -o $@


${PYBIND11_LIBRARY}: ${LIBRARY_ATABLE_SO_FILE} ${LIBRARY_EXCEPTION_SO_FILE} ${PYBIND11_OBJECT_FILES}
	mkdir -p $(dir $@) 
	${CXX} ${CPP_FLAGS} ${LINK_LIBRARY_FLAGS} ${LINK_LIBRARIES} $(shell python3-config --ldflags) -export-dynamic  -ldsat ${PYBIND11_OBJECT_FILES} -o $@



${LIBRARY_EXCEPTION_SO_FILE}:
	${MAKE} -C external/Exception/

obj/%.o : src/%.cc
	mkdir -p $(dir $@)
	mkdir -p {lib,obj}
	g++ ${CPP_FLAGS} -c $< -o $@

