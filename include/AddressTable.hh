#ifndef __ADDRESSTABLE_HPP__
#define __ADDRESSTABLE_HPP__

#include "AddressTableException.hh"
#include <cstddef>
#include <string>
#include <vector>
#include <unordered_map>
#include <map>

#include <boost/tokenizer.hpp> //tokenizer

#include <stdint.h>

#include <Item.hh>

class AddressTable : private Item{
 public:
  AddressTable(std::string const & addressTableName);
  ~AddressTable();
  Item const *              GetItem     (std::string const &);
  std::vector<Item const *> GetTagged   (std::string const & tag);
  std::vector<std::string>  GetNames    (std::string const & regex = "*");
  std::vector<std::string>  GetAddresses(uint16_t lower, uint16_t upper);
  std::vector<std::string>  GetTables   (std::string const &regex);
  std::vector<Item const *> GetChildren ();
 private:  
  //default constructor is forbidden 
  AddressTable();
  //preventcopying
  AddressTable( const AddressTable & );
  AddressTable& operator=(const AddressTable &);

  int fileLevel;  //Should move this to a passed reference
  std::string topPath;
  void LoadFile(std::string const &,
		std::string const & prefix,
		uint32_t offset,
		Item * itemParent);
  void ProcessLine(std::string const &,
		   size_t,
		   std::string const & prefix = "",
		   uint32_t offset=0,
		   Item * parent = NULL);
  Item * FindExisting(Item *,std::string const &);
  void ProcessName(Item * item,
		   std::string const & prefix,
		   std::string name);
  void ProcessAddress(Item * item,
		      uint32_t offset,
		      std::string const & value);
  void ProcessMask(Item * item,
		   std::string const & value);
  void ProcessInclude(Item * item,
		      std::string filename,
		      Item * itemParent);
  void ProcessMode(Item * item,
		   std::string const & value);
  void ProcessUser(Item * item,
		   boost::tokenizer<boost::char_separator<char> > tokens,
		   boost::tokenizer<boost::char_separator<char> >::iterator & itToken,
		   size_t & iToken,
		   std::string const & activeLine,
		   size_t lineNumber);




  //Ds of entries
  //req  req     req  req  tokenized
  //name address mask mode user
  void AddEntry(Item *);
  //Map of address to Items (master book-keeping; delete from here)
  std::map<uint32_t,std::vector<Item*> > addressItemMap;
  //Map of names to Items
  std::map<std::string,Item*> nameItemMap;
  //  std::vector<Item *> children;
  static const char * SC_key_conv;

};
#endif
