#ifndef __ITEM_HH__
#define __ITEM_HH__ 1

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

class AddressTable;


class Item{
public:
  Item();
  enum ModeMask {
    EMPTY  = 0x0,
    READ   = 0x1,
    WRITE  = 0x2,
    ACTION = 0x4
  };
  std::string const & GetName()                                 const;
  std::string         GetFullName(size_t recursionLevels = 20)  const;
  std::string         GetParentName()                           const;
  uint32_t            GetAddress()                              const;
  uint32_t            GetMask()                                 const;
  uint8_t             GetOffset()                               const;
  uint8_t             GetMode()                                 const; 
  std::unordered_map<std::string,std::string> const & GetUser() const;
  std::vector<Item const *> const GetChildren()                 const;
  Item const * GetParent()                                      const;
  friend class AddressTable;
private:
  std::string name;
  uint32_t address;
  uint32_t mask;
  uint8_t  offset;
  uint8_t  mode; // r :0, w :1, a:2, i:3
  std::unordered_map<std::string,std::string> user;
  Item * parent;
  std::vector<Item *> children;
};

#endif
