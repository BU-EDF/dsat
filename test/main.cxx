
#include <AddressTable.hh>
#include <Exception/ExceptionBase.hh>



void PrintChildren(std::vector<Item const *> const children,std::string padding=""){
  //Find max name length
  size_t maxNameLength = 0;
  for(auto it = children.begin();
      it != children.end();
      it++){
    size_t nameLength = (*it)->GetName().size();
    if(nameLength > maxNameLength){
      maxNameLength= nameLength;
    }
  }

  std::string newPadding(maxNameLength,' ');
  newPadding += padding;
  
  //Print names
  for(auto it = children.begin();
      it != children.end();
      it++){
    printf("%s%s\n",padding.c_str(),(*it)->GetName().c_str());
    PrintChildren((*it)->GetChildren(),newPadding);
  }
}

int main(int argc, char ** argv){

  if(argc < 2){
    fprintf(stderr,"Missing filename\n");
  }

  
  AddressTable * at;
  try{
    at = new AddressTable(argv[1]);
  }catch (Exception::exBase & e){
    fprintf(stderr,"Exception: %s\n",e.Description());
    return -1;
  }

  auto regs= at->GetNames("*");
  int i = 0;

  printf("================================================================================\n");
  printf("All Names\n");
  printf("================================================================================\n");
  for(auto itReg = regs.begin();
      itReg != regs.end();
      itReg++){
    Item const * item = at->GetItem(*itReg);
    printf("%-50s: 0x%08X (0x%08X) %01X\n",
	   itReg->c_str(),
	   item->GetAddress(),
	   item->GetMask(),
	   item->GetMode()
	   );
    i++;
  }


  printf("================================================================================\n");
  printf("All Levels\n");
  printf("================================================================================\n");
  PrintChildren(at->GetChildren());

  return 0;
}
