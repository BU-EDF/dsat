#!/usr/bin/env python3

import sys
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, '../lib')

try:
    from libdsat import *
except ModuleNotFoundError:
    print("libdsat module not found. Are the Python bindings installed?")
    sys.exit(1) 

at = AddressTable("tables/Carrier.adt")
    
names = at.GetNames("*")

for name in names:
    item = at.GetItem(name)
    print("%-50s: 0x%08X (0x%08X) %01X" % (name,
                                           item.GetAddress(),
                                           item.GetMask(),
                                           item.GetMode()))
